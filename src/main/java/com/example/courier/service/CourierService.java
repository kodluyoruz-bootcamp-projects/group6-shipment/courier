package com.example.courier.service;

import com.example.courier.domain.Courier;
import com.example.courier.exception.CourierNotCreatedException;
import com.example.courier.exception.CourierNotFoundException;
import com.example.courier.repository.CourierRepository;
import org.springframework.stereotype.Service;

@Service
public class CourierService {
    private final CourierRepository courierRepository;

    public CourierService(CourierRepository courierRepository) {
        this.courierRepository = courierRepository;
    }

    public void createAppointment(Courier courier) throws CourierNotCreatedException {
        courierRepository.insert(courier);
    }

    public String getDate(String id) throws CourierNotFoundException {
       return courierRepository.getDate(id);
    }

    public void updateCourier(String id, String date) throws CourierNotFoundException {
        courierRepository.update(id,date);
    }
}
