package com.example.courier.service;

import com.example.courier.domain.Courier;
import com.example.courier.domain.Person;
import com.example.courier.exception.CourierNotCreatedException;
import com.example.courier.exception.CourierNotFoundException;
import com.example.courier.repository.CourierRepository;
import com.example.courier.service.CourierService;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CourierServiceTest {

    @Mock
    private CourierRepository courierRepository;

    @InjectMocks
    private CourierService courierService;

    @SneakyThrows
    @Test
    public void update_should_throw_CourierNotFoundException_when_courier_id_is_not_registered() {

        // Arrange
        String CourierId = "123";
        String newDate = "28 Ekim 2020 10:00";
        doThrow(CourierNotFoundException.class).when(courierRepository).update(any(String.class),any(String.class));

        // Act
        Throwable throwable = catchThrowable(() -> courierService.updateCourier(CourierId,newDate));

        //Assert
        assertThat(throwable).isInstanceOf(CourierNotFoundException.class);
    }

    @SneakyThrows
    @Test
    public void insert_should_throw_CourierNotCreatedException_when_courier_is_not_valid() {

        // Arrange
        Courier sut = new Courier();
        doThrow(CourierNotCreatedException.class).when(courierRepository).insert(any(Courier.class));

        // Act
        Throwable throwable = catchThrowable(() -> courierService.createAppointment(sut));

        //Assert
        assertThat(throwable).isInstanceOf(CourierNotCreatedException.class);
    }


    @SneakyThrows
    @Test
    public void getDate_should_throw_CourierNotFoundException_when_id_is_not_registered() {

        // Arrange
        String id = "123";
        when(courierRepository.getDate(id)).thenThrow(new CourierNotFoundException("Courier Not Found"));

        // Act
        Throwable throwable = Assertions.catchThrowable(() -> courierService.getDate(id));

        // Assert
        assertThat(throwable).isInstanceOf(CourierNotFoundException.class).hasMessage("Courier Not Found");
    }

}
